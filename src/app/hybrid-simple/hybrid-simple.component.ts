import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hybrid-simple',
  template: '<div class="view-container"><div ng-view class="view-frame"></div></div>',
})
export class HybridSimpleComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
