import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HybridSimpleComponent } from './hybrid-simple.component';

describe('HybridSimpleComponent', () => {
  let component: HybridSimpleComponent;
  let fixture: ComponentFixture<HybridSimpleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HybridSimpleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HybridSimpleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
