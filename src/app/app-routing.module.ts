import { NgModule } from '@angular/core';
import {RouterModule, Routes, UrlSegment} from '@angular/router';
import {HybridSimpleComponent} from "./hybrid-simple/hybrid-simple.component";
import {HomeComponent} from "./home/home.component";

// Match any URL that starts with `users`
export function isAngularJSUrl(url: UrlSegment[]) {
  return url.length > 0 && url[0].path.startsWith('phones') ? ({consumed: url}) : null;
}

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    matcher: isAngularJSUrl,
    component: HybridSimpleComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
