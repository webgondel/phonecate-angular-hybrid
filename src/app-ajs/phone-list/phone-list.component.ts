'use strict';

// Register `phoneList` component, along with its associated controller and template
angular.
  module('phoneList').
  component('phoneList', {
    template: '<div class="container-fluid">\n' +
      '  <div class="row">\n' +
      '    <div class="col-md-2">\n' +
      '      <!--Sidebar content-->\n' +
      '\n' +
      '      <p>\n' +
      '        Search:\n' +
      '        <input ng-model="$ctrl.query" />\n' +
      '      </p>\n' +
      '\n' +
      '      <p>\n' +
      '        Sort by:\n' +
      '        <select ng-model="$ctrl.orderProp">\n' +
      '          <option value="name">Alphabetical</option>\n' +
      '          <option value="age">Newest</option>\n' +
      '        </select>\n' +
      '      </p>\n' +
      '\n' +
      '    </div>\n' +
      '    <div class="col-md-10">\n' +
      '      <!--Body content-->\n' +
      '\n' +
      '      <ul class="phones">\n' +
      '        <li ng-repeat="phone in $ctrl.phones | filter:$ctrl.query | orderBy:$ctrl.orderProp"\n' +
      '            class="thumbnail phone-list-item">\n' +
      '          <a href="#!/phones/{{phone.id}}" class="thumb">\n' +
      '            <img ng-src="{{phone.imageUrl}}" alt="{{phone.name}}" />\n' +
      '          </a>\n' +
      '          <a href="#!/phones/{{phone.id}}">{{phone.name}}</a>\n' +
      '          <p>{{phone.snippet}}</p>\n' +
      '        </li>\n' +
      '      </ul>\n' +
      '\n' +
      '    </div>\n' +
      '  </div>\n' +
      '</div>\n',
    controller: ['Phone',
      function PhoneListController(Phone) {
        this.phones = Phone.query();
        this.orderProp = 'age';
      }
    ]
  });
